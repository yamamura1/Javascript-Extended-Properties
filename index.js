const fs = require('fs');
const path = require("path");

const { List } = require('list');
const { inspect } = require('util')

let mainDirScan = fs.readdirSync(path.join(__dirname))
	.filter(item => !item.includes('.'))

for (let curDirectory of mainDirScan) {
	let scanSubDir = fs.readdirSync(path.join(__dirname, curDirectory));

	for (let currentFile of scanSubDir) {
		let currentFunction = inspect(currentFile.split('.').slice(0, -1).join('.'));
		eval(`Object.defineProperty(${curDirectory}.prototype, "${currentFunction}", { value: require('./${curDirectory}/${currentFile}'), enumerable: false});`);
	}
}
