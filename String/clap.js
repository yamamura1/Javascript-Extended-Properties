const { chunk } = require('../extra')

/**
 * Mock someone's words. Can be randomized or not
 * @example "hi".mock() => "hI"
 * @returns {string}
 */
module.exports = function clap() {
	let target = this;

	if (!target.includes(" "))
		return chunk(target, 1).join(" 👏 ")

	return target.replace(/\s+/gmi, " 👏 ")
};
